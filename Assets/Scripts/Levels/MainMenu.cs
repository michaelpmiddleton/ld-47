﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
/*
    This is a template for how to abstract the LevelManager on a level-level basis
*/

public class MainMenu : LevelManager {
    [SerializeField]
    private TextMeshProUGUI cheekyLabel;
    [SerializeField]
    private SingleTeleporter loopCompleter;



    protected override void Awake() {
        base.Awake();
        timerIsActive = false;
    }

    public override void BeginInteraction() {
        Pause();
        interactable.Focus();
    }

    public override void LoadLevelData(Scene scene, LoadSceneMode loadSceneMode) {
        base.LoadLevelData(scene, loadSceneMode);
        GameManager.Sound.Play("Music");
        if (GameManager.Completion > 0) {
            Player.transform.position = loopCompleter.transform.position;
            cheekyLabel.text = "You did it!";
        }
    }
}