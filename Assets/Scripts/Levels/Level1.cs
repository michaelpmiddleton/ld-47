﻿using UnityEngine;
using UnityEngine.SceneManagement;
/*
    This is a template for how to abstract the LevelManager on a level-level basis
*/

public class Level1 : LevelManager {
    protected override void ResetLevel() {
        this.Player.transform.position = spawnPosition.position;
        base.ResetLevel();
    }

    public override void BeginInteraction() {
        Pause();
        interactable.Focus();
    }

    public override void LoadLevelData(Scene scene, LoadSceneMode loadSceneMode) {
        base.LoadLevelData(scene, loadSceneMode);
    }
}