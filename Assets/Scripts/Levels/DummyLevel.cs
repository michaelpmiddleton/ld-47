﻿using UnityEngine;
using UnityEngine.SceneManagement;
/*
    This is a template for how to abstract the LevelManager on a level-level basis
*/

public class DummyLevel : LevelManager {
    [SerializeField]
    private Interactable interactable;

    protected override void ResetLevel() {
        this.Player.transform.position = spawnPosition.position;
        base.ResetLevel();
    }

    public override void BeginInteraction() {
        Pause();
        interactable.Focus();
    }

    public override void LoadLevelData(Scene scene, LoadSceneMode loadSceneMode){
        GameManager.Level = this;
        Debug.Log("Loaded level DUMMY LEVEL");
    }
}