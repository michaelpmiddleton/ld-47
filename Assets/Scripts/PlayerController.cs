﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour {
    public Camera cam;
    public NavMeshAgent agent;
    public Collider InteractableDetector;
    public Animator animator;
    public AudioSource audioSource;

    void Update() {
        if (Input.GetMouseButtonDown(0) &&
        EventSystem.current.currentSelectedGameObject == null &&
        GameManager.Level.State == LevelManager.LevelState.DEFAULT) {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit)) {
                agent.SetDestination(hit.point);
            }
        }

        if (agent.velocity.sqrMagnitude == 0){
            animator.SetBool("isMoving", false);

            if (audioSource.isActiveAndEnabled) {
                audioSource.enabled = false;
            }
        } else {
            animator.SetBool("isMoving", true);

            if (!audioSource.isActiveAndEnabled) {
                audioSource.enabled = true;
            }
        }
    }
}
