﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelOne_ForceField : LevelObject {
    [SerializeField]
    private SingleTeleporter linkedTP;

    public override bool PerformInteraction(LevelObjectInteraction loi) {
        gameObject.SetActive(false);
        GameManager.Level.RenderNavMesh();
        loi.Animation();
        loi.Sound();
        linkedTP.PerformInteraction(loi);
        return false;
    }
}
