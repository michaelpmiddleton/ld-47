﻿using UnityEngine;

public class LevelOne_Door : LevelObject {
    private AnimationEvent oae;


    // Unity functions //////////////////////////////////////////////
    void Start() {
        // Initialize event that will run at the end of the the animation
        oae = new AnimationEvent();
        oae.intParameter = 11111;
        oae.time = 1.0f;
        oae.functionName = "OnAnimationEnd";

        // Attach the event to the door open/close animations
        ObjectAnimator.runtimeAnimatorController.animationClips[2].AddEvent(oae);
        ObjectAnimator.runtimeAnimatorController.animationClips[3].AddEvent(oae);
    }
    // ///////////////////////////////////////////////////////////////////////////


    // Inherited/Overriden functions //////////////////////////////////////////////
    // Set the position of the stairs and rebake navigation
    public override bool PerformInteraction(LevelObjectInteraction loi){
        loi.Animation();
        loi.Sound();
        return false;
    }
    // ///////////////////////////////////////////////////////////////////////////


    // LEVELONE_DOOR-specific code ///////////////////////////////////////////////
    public void OnAnimationEnd(int value) {
        GameManager.Level.RenderNavMesh();
    }
}
