﻿using UnityEngine;

/*
    This is a template for how to abstract LevelObjects individually. Any
    object-specific information should appear in this level of script.
*/

public class DummyLevelObject : LevelObject {
    public override bool PerformInteraction(LevelObjectInteraction loi) {
        loi.Animation();
        Debug.Log("Something happened!");
        return false;
    }
}
