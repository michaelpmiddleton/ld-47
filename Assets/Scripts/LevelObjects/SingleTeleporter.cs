﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class SingleTeleporter : LevelObject {
    [SerializeField]
    private string TeleportDestination;
    [SerializeField]
    private bool isActive = false;
    [SerializeField]
    private bool incrementsCounter;

    public ParticleSystem activeParticles;
    public ParticleSystem passiveParticles;


    // Unity Functions //////////////////////////////////////////////////////////
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && isActive) {
            activeParticles.Play();
            GameManager.Sound.Play("Teleport");
            if (incrementsCounter) GameManager.Completion++;
            // Add IEnum call here, set to like 1 second. Put Scene Manager in there
            StartCoroutine("Wait");
        }
    }
    // //////////////////////////////////////////////////////////////////////////

    // Inherited/Overriden Functions  ///////////////////////////////////////////
    public override bool PerformInteraction(LevelObjectInteraction loi) {
        loi.Animation();
        loi.Sound();
        isActive = true;
        passiveParticles.Play();
        return false;
    }
    // //////////////////////////////////////////////////////////////////////////

    IEnumerator Wait(){
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(TeleportDestination);
    }

}
