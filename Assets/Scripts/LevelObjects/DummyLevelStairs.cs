﻿using UnityEngine;
// using UnityEditor.AI;

public class DummyLevelStairs : LevelObject {

    public Vector3 goalTransform = new Vector3(0,0,0);
    
    public override bool PerformInteraction(LevelObjectInteraction loi){
        transform.position = goalTransform;
        // NavMeshBuilder.BuildNavMesh();
        return true;
    }
}
