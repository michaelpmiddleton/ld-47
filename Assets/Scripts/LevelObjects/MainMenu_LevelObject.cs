using UnityEngine;
using UnityEngine.SceneManagement;

/*
    This is a template for how to abstract LevelObjects individually. Any
    object-specific information should appear in this level of script.
*/

public class MainMenu_LevelObject : LevelObject {
    [SerializeField]
    private float delayTime;
    private bool delayTriggered = false;
    private bool wasPlay, wasQuit;


    public override bool PerformInteraction(LevelObjectInteraction loi) {
        loi.Animation();
        delayTriggered = true;
        wasPlay = loi is MainMenu_PlayButton;
        wasQuit = loi is MainMenu_QuitButton;
        return false;
    }

    void Update() {
        if (delayTriggered) {
            if (delayTime < 0) {
                if (wasPlay)
                    SceneManager.LoadScene("Level 1");

                if (wasQuit)
                    Application.Quit();
            }

            delayTime -= Time.deltaTime;
        }
    }
}
