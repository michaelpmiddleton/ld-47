﻿using UnityEngine;
// using UnityEngine.AI;

public class LevelOne_Stairs : LevelObject {
    private AnimationEvent oae;
    public int index1;
    public int index2;
    // This is the end position for the staircase in the level
    //public Vector3 goalTransform = new Vector3(0,0,0);
    
    // Unity functions //////////////////////////////////////////////
    void Start() {
        // Initialize event that will run at the end of the the animation
        oae = new AnimationEvent();
        oae.intParameter = 11112;
        oae.time = 1.0f;
        oae.functionName = "OnAnimationEnd";

        // Attach the event to the door open/close animations
        ObjectAnimator.runtimeAnimatorController.animationClips[index1].AddEvent(oae);
        ObjectAnimator.runtimeAnimatorController.animationClips[index2].AddEvent(oae);
    }
    // Set the position of the stairs and rebake navigation
    public override bool PerformInteraction(LevelObjectInteraction loi){
        //transform.position = goalTransform;
        loi.Animation();
        loi.Sound();
        return true;
    }

    // LEVELONE_DOOR-specific code ///////////////////////////////////////////////
    public void OnAnimationEnd(int value) {
        GameManager.Level.RenderNavMesh();
    }
}
