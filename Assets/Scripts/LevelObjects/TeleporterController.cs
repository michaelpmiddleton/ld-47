﻿using UnityEngine;
using UnityEngine.AI;

public class TeleporterController : LevelObject {
    // Fields
    public Transform otherTeleporter;
    public Transform spawn;
    public NavMeshAgent navMeshAgent;
    private bool isActive = false;

    public ParticleSystem startPS;
    public ParticleSystem endPS;

    public ParticleSystem passiveStartPs;
    public ParticleSystem passiveEndPs;


    // Unity Functions //////////////////////////////////////////////////////////
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !other.isTrigger && isActive) {

            navMeshAgent.enabled = false;
            other.transform.position = otherTeleporter.transform.position;

            navMeshAgent.enabled = true;
            navMeshAgent.SetDestination(otherTeleporter.transform.position);

            spawn.position = otherTeleporter.position;

            GameManager.Sound.Play("Teleport");
            startPS.Play();
            endPS.Play();
        }
    }
    // //////////////////////////////////////////////////////////////////////////

    // Inherited/Overriden Functions  ///////////////////////////////////////////
    public override bool PerformInteraction(LevelObjectInteraction loi) {
        loi.Animation();
        loi.Sound();

        passiveStartPs.Play();
        passiveEndPs.Play();

        if (loi is LevelOne_TeleporterValve)
            isActive = ((Valve)loi.Agent).GetStage() == 2; // If we're in the red

        else if (loi is LevelOne_TeleporterPP)
            isActive = true;

        else {
            Debug.LogError("Someone else is calling Teleporter.PerformInteraction");
            Debug.Log(loi);
        }


        return false;
    }
    // //////////////////////////////////////////////////////////////////////////

}
