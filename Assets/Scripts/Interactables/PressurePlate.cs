﻿using UnityEngine;
using System.Collections;

public class PressurePlate : Interactable {
    public LevelObject intermediateLevelObject;
    public LevelObjectInteraction intermediateInteraction;
    [SerializeField]
    private float PressureDuration;
    [SerializeField]
    private float timer;
    private bool isActive;

    public override void CompleteInteraction() {
        // Depress the pressure plate
        isActive = true;
        
        intermediateLevelObject.PerformInteraction(intermediateInteraction);
        
        // ALWAYS call the main 
        base.CompleteInteraction();

        StartCoroutine("Reset");
    }


    protected override void OnTriggerEnter(Collider other) {
        if (!isActive && other == GameManager.Level.Player.InteractableDetector) {
            CompleteInteraction();
        }
    }

    void Awake() {
        timer = PressureDuration;
    }
    void Update() {
        if (isActive) {
            timer -= Time.deltaTime;

            if (timer <= 1) {
                // Deactivate the bridge
                intermediateLevelObject.PerformInteraction(intermediateInteraction);

                // Reset timer and set to inactive
                isActive = false;
                timer = PressureDuration;
            }
        }
    }

    IEnumerator Reset(){
        yield return new WaitForSeconds(timer - 1);
        isActive = false;
        gameObject.GetComponent<Animator>().SetTrigger("PressureTrigger");
    }
}
