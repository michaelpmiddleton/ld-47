﻿using UnityEngine;

public class Valve : Interactable {
    public LevelObject intermediateLevelObject;
    public LevelObjectInteraction intermediateInteraction;
    [SerializeField]
    private int stage = 0;

    public override void CompleteInteraction() {
        // Only mess with the door going GREEN -> YELLOW
        if (stage == 0)
            intermediateLevelObject.PerformInteraction(intermediateInteraction);
        
        // Progress through the stages
        stage++;
        stage %= 3;

        // ALWAYS call the main 
        base.CompleteInteraction();
    }

    public int GetStage() {
        return stage;
    }
}
