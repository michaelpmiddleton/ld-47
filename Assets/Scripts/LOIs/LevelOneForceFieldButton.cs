﻿using UnityEngine;

public class LevelOneForceFieldButton : LevelObjectInteraction {
    public LevelOneForceFieldButton(Interactable agent) : base(agent) { }
    public override void Animation() {
        animator.SetTrigger("ButtonTrigger");
    }

    public override void Sound() {
        GameManager.Sound.Play("Button Press");
    }
}
