﻿using UnityEngine;

public class LevelOne_StairButton : LevelObjectInteraction {
    public LevelOne_StairButton (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("ButtonTrigger");
        Agent.levelObject.ObjectAnimator.SetTrigger("StairsTrigger");
    }

    public override void Sound(){
        GameManager.Sound.Play("Button Press");
        GameManager.Sound.Play("Motor");   
    }
}
