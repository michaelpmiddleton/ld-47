﻿using UnityEngine;

public class LevelOne_StairPP : LevelObjectInteraction {
    public LevelOne_StairPP (Interactable agent) : base(agent){}
    public override void Animation() {
        //animator.SetTrigger("ButtonTrigger");
        ((PressurePlate)Agent).intermediateLevelObject.ObjectAnimator.SetTrigger("StairsTrigger");
    }

    public override void Sound(){
        GameManager.Sound.Play("Pressure Plate");
        GameManager.Sound.Play("Motor");
    }
}
