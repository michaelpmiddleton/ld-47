﻿using UnityEngine;

public class MainMenu_PlayButton : LevelObjectInteraction {
    public MainMenu_PlayButton (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("ButtonTrigger");
    }
}
