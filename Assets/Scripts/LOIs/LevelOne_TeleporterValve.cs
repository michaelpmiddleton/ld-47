﻿using UnityEngine;

public class LevelOne_TeleporterValve : LevelObjectInteraction {
    public LevelOne_TeleporterValve (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("WheelTrigger");
        // Agent.levelObject.ObjectAnimator.SetTrigger("OpenSesame");
    }

    public override void Sound(){
        GameManager.Sound.Play("Turn Wheel");
    }
}
