﻿using UnityEngine;

public class LevelOne_DoorLever : LevelObjectInteraction {
    public LevelOne_DoorLever (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("FlipSwitch");
        Agent.levelObject.ObjectAnimator.SetTrigger("OpenSesame");
    }
    public override void Sound(){
        GameManager.Sound.Play("Lever Flip");
        GameManager.Sound.Play("Motor");
    }
}
