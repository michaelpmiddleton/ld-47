using UnityEngine;

public class MainMenu_Teleporter : LevelObjectInteraction {
    public MainMenu_Teleporter (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("ButtonTrigger");
    }
}
