﻿using UnityEngine;

public class MainMenu_QuitButton : LevelObjectInteraction {
    public MainMenu_QuitButton (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("ButtonTrigger");
    }
}
