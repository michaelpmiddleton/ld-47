﻿using UnityEngine;

public class LevelOne_TeleporterPP : LevelObjectInteraction {
    public LevelOne_TeleporterPP (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("PressureTrigger");
        // Agent.levelObject.ObjectAnimator.SetTrigger("OpenSesame");
    }
}
