﻿using UnityEngine;

public class LevelOne_DoorValve : LevelObjectInteraction {
    public LevelOne_DoorValve (Interactable agent) : base(agent){}
    public override void Animation() {
        animator.SetTrigger("WheelTrigger");
        ((Valve)Agent).intermediateLevelObject.ObjectAnimator.SetTrigger("OpenSesame");
    }
}
