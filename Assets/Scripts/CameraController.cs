﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float xOffset = 6f;
    public float zOffset = 6f;
    public Transform player;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x - xOffset, transform.position.y, player.position.z - zOffset);
    }
}
