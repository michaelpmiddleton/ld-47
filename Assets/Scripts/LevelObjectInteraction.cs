﻿using UnityEngine;

public abstract class LevelObjectInteraction : MonoBehaviour {
    public Interactable Agent;
    protected Animator animator;
    public LevelObjectInteraction(Interactable agent) {}
    public abstract void Animation();
    public Interactable GetAgent() {
        return Agent;
    }

    void Awake() {
        Agent = GetComponent<Interactable>();
        animator = GetComponent<Animator>();
    }

    virtual public void Sound(){
        
    }
}
