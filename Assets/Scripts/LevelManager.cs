﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public abstract class LevelManager : MonoBehaviour {
    public Transform spawnPosition;
    public enum LevelState { DEFAULT = 0, INTERACTABLE = 1}
    public Interactable RelevantInteractable { get {return interactable; } set { interactable = value; } }
    [SerializeField]
    private NavMeshSurface navigableSurface;
    public LevelState State;
    public string LevelName;
    public UIManager LevelUI;
    public PlayerController Player;
    [SerializeField]
    private Camera InteractableCamera;
    private Camera mainCamera;
    protected Interactable interactable;
    private Scene level;
    [SerializeField]
    private Interactable intCameraTarget;
    [SerializeField]
    private float focusTime;
    private float timer;
    private float MAX_CAMERA_OFFSET = 0.5f;
    public int LoopDuration;
    protected bool timerIsActive;
    

    protected virtual void Awake() {
        State = LevelState.DEFAULT;
        timer = LoopDuration;
        timerIsActive = true;
        mainCamera = Camera.main;
    }

    void OnEnable() {
        // Add custom level loading
        SceneManager.sceneLoaded += LoadLevelData;
    }

    void OnDisable() {
        // Remove the custom level loading from the scene manager
        SceneManager.sceneLoaded -= LoadLevelData;
    }

    protected virtual void Update() {
        if (timerIsActive) {
            timer -= Time.deltaTime;
            

            if (timer <= 0f)
                ResetLevel();
            
            //TODO: Change to be more visually pleasing!
            LevelUI.UpdateTimerText((int)timer);
        }
        if (Camera.main == mainCamera) {
            if(Input.GetKeyUp(KeyCode.Space) && interactable != null) {
                BeginInteraction();
            }
        }
        else if (Camera.main == InteractableCamera) {
            switch(State) {
                case LevelState.DEFAULT:
                    // Move the camera smoothly to main camera position target
                    InteractableCamera.transform.position = Vector3.Lerp(
                        InteractableCamera.transform.position,
                        mainCamera.transform.position,
                        Time.deltaTime * focusTime
                    );
                    InteractableCamera.transform.LookAt(Player.transform);                    
                    
                    // If the camera is close enough to the main camera, reactivate main camera and disable this one
                    if (mainCamera.transform.position.magnitude - InteractableCamera.transform.position.magnitude <= MAX_CAMERA_OFFSET) {
                        mainCamera.enabled = true;
                        InteractableCamera.enabled = false;
                        Play();
                    }
                    break;

                case LevelState.INTERACTABLE: 
                    // Move the camera smoothly to main camera position target
                    InteractableCamera.transform.position = Vector3.Lerp(
                        InteractableCamera.transform.position,
                        intCameraTarget.FocusCameraPosition.position,
                        Time.deltaTime * focusTime
                    );
                    InteractableCamera.transform.LookAt(intCameraTarget.transform);
                    break;
            }
        }       
    }

    public void FocusOnInteractable(Interactable target) {
        // Set interactable camera to same location is primary
        InteractableCamera.transform.SetPositionAndRotation(mainCamera.transform.position, mainCamera.transform.rotation);
        
        // Swap the enabled cameras
        InteractableCamera.enabled = true;
        mainCamera.enabled = false;

        // set target for camera to track towards
        intCameraTarget = target;

        // Prevent the player from moving while messing with interactable
        State = LevelState.INTERACTABLE;
    }

    public void FocusOnOverworld() {
        // Allow overworld interaction again
        State = LevelState.DEFAULT;
    }

    public void RenderNavMesh() {
        if (navigableSurface.navMeshData != null)
            navigableSurface.RemoveData();
        navigableSurface.BuildNavMesh();
    }
    public void ClearInteractable() {
        interactable = null;
    }
    protected void Play() {
        timerIsActive = true;
    }

    protected void Pause() {
        timerIsActive = false;
    }

    public virtual void LoadLevelData(Scene scene, LoadSceneMode loadSceneMode) {
        GameManager.Level = this;
        RenderNavMesh();
        Debug.Log($"Loaded level - {scene.name} ({this.LevelName})");
    }    
    public abstract void BeginInteraction();
    virtual protected void ResetLevel() {
        timer = LoopDuration;
    }
}
