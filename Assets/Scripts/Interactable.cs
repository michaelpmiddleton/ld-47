﻿using UnityEngine;

public abstract class Interactable : MonoBehaviour {
    public LevelObject levelObject;
    public LevelObjectInteraction interaction;
    public Transform FocusCameraPosition;
    protected bool hasFocus = false;


    // Interactable functions
    public void Focus() {
        GameManager.Level.FocusOnInteractable(this);
        hasFocus = true;
    }
    public void Defocus() {
        GameManager.Level.FocusOnOverworld();
        hasFocus = false;
    }
    public virtual void CompleteInteraction() {
        bool escapeAfterComplete = this.levelObject.PerformInteraction(interaction);
        if (escapeAfterComplete)
            Defocus();
    }


    // Unity Functions
    void Update() {
        if (hasFocus) {
            if (Input.GetKeyUp(KeyCode.Return))
                CompleteInteraction();
            else if (Input.GetKeyUp(KeyCode.Escape))
                Defocus();
        } 
    }
    protected virtual void OnTriggerEnter(Collider other) {
        if (other == GameManager.Level.Player.InteractableDetector) {
            GameManager.Level.RelevantInteractable = this;
        }
    }
    void OnTriggerExit(Collider other) {
        if (other == GameManager.Level.Player.InteractableDetector) {
            GameManager.Level.ClearInteractable();
        }
    }
}
