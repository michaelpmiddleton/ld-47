﻿using UnityEngine;

public abstract class LevelObject : MonoBehaviour {
    public Animator ObjectAnimator;
    
    // Returns true if you should immediately return to overworld on completion of function
    public abstract bool PerformInteraction(LevelObjectInteraction loi);

    void Awake() {
        ObjectAnimator = GetComponent<Animator>();
    }
}
