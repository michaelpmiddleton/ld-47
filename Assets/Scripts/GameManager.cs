﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private static GameManager gm;
    public static LevelManager Level { get {return gm.level; } set {gm.level = value;} }
    private LevelManager level;
    public static AudioManager Sound { get {return gm.sound; } }
    private AudioManager sound;
    public static int Completion { get {return gm.completions; } set { gm.completions = value;} }
    private int completions;

    void Awake() {
        // Singleton Code Block //////////////////////////////////////
        if (gm == null) {
            gm = this;
            gm.sound = GetComponent<AudioManager>();
            gm.completions = 0;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (gm != this)
            Destroy(gm.gameObject);
        // ///////////////////////////////////////////////////////////

    }

    void Update() {
        if (Input.GetKeyUp(KeyCode.Q)) {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
