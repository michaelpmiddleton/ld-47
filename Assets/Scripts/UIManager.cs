﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour {
    public TextMeshProUGUI Timer;
    public string TimerText;
    [SerializeField]
    private Button interactButton;

    public void UpdateTimerText(int number) {
        Timer.SetText(System.String.Format(TimerText, number));
    }
}
